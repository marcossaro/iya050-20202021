const fetch = require('node-fetch');
const {x_application_id} = 'r3d2a595-389b-43e2-a535-0ba7a480f57c';

const url = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";

const createAPair = (id, body) => {
    console.log("entre en el createAPair");
    console.log(body);
    console.log(id);
    fetch(`${url}/pairs/${id}`,
        {
            method: 'PUT',
            headers: {"x-application-id": x_application_id},
            body: JSON.stringify(body)
        })
        .then(response => response.json())
        .catch( error => "createAPair : error");
}

const readPair = (id) => {

    return fetch(`${url}/pairs/${id}`,
        {
            method: 'GET',
            headers: {"x-application-id": x_application_id}
        })
        .then(response => response)
        .then(response => response.json())
        .catch( error => "readPair : error");
}

const readAllPair = () => {

    return fetch(`${url}/pairs`,
        {
            method: 'GET',
            headers: {"x-application-id": x_application_id}
        })
        .then(response => response.json())
        .catch( error => "readAllPair : error");

}

module.exports={createAPair,readPair,readAllPair}