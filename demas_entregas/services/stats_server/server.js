const { GraphQLServer } = require("graphql-yoga");

const fetch = require("node-fetch");

const fetchs = require('./fetchs.js');

const schema = `
  type Query {
    partida(id: ID!): Partida
  }
  type Partida {
    id: ID
    nombre: String
    fechaHora: String
    puntos: String
    muertes: String
    tiempo: String
    record: String 
    estado: String
  }
  type Mutation {
    crearPartida(id: ID, nombre: String, key: String, fechaHora: String, puntos: String, muertes: String, tiempo: String, record: String, estado: String) : String
    cambiarEstado(id: ID, key: String, estado: String) : String
  }

`;

const resolvers = {
  Query: {
    partida: async(_, { id }) => fetchs.readPair(id).then(res => res),
  },

  Partida: {
    id: (data) => JSON.parse(data.value).id,
    nombre: (data) => JSON.parse(data.value).nombre,
    fechaHora : (data) => JSON.parse(data.value).fechaHora,
    puntos: (data) => JSON.parse(data.value).puntos,
    muertes: (data) => JSON.parse(data.value).muertes,
    tiempo : (data) => JSON.parse(data.value).tiempo,
    record: (data) => fetchs.readAllPair().then(partidas => {
           
      let mejor_jugador = "Record";
      let puntosRecord = '0';
      partidas.map(partida => {

          const partidaValue = JSON.parse(partida.value);

          if (partidaValue.puntos > puntosRecord) {
            mejor_jugador = partidaValue.nombre;
          };

          console.log(partida.value);
      })
      return mejor_jugador;
    }),
    estado : (data) => JSON.parse(data.value).estado,   
  },

  Mutation: {
    crearPartida: (_, args) => fetchs.readPair(args.key).then(partida => {

          const body = {
            "id" : args.id,
            "nombre" : args.nombre,
            "fechaHora" : args.fechaHora,
            "puntos" : args.puntos,
            "muertes" : args.muertes,
            "tiempo" : args.tiempo,
            "estado" : args.estado,                 
           };
            console.log(body);
            fetchs.createAPair(args.key, body);
    }),

    cambiarEstado: (_, args) => fetchs.readPair(args.key).then(partida => {

      const body = {
        "id" : args.id,
        "nombre" : args.nombre,
        "fechaHora" : args.fechaHora,
        "puntos" : args.puntos,
        "muertes" : args.muertes,
        "tiempo" : args.tiempo,
        "estado" : args.estado,                 
       };
        console.log(body);
        fetchs.createAPair(args.key, body);
}),
  },
}

const server = new GraphQLServer({ 
  typeDefs: schema, 
  resolvers,
});

const opts = {
  cors: {
    credentials: true,
    origin: ["*"],
  },

  port: process.env.PORT,
  playground: "/",

};

server.start(opts).then(console.log("Servidor Stats GraphQL"));