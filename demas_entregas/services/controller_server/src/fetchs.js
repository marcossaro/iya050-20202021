const fetch = require('node-fetch');

const crearPartida = (id, nombre, key, fecha, estado) => {
    return fetch(`${process.env.STATS_SERVER_URL}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body :
            JSON.stringify({
              query: `mutation {crearPartida(id: "${id}", nombre: "${nombre}", key: "${key}", fechaHora: "${fecha}", puntos: "", muertes:"", tiempo:"", estado:"${estado}")}`
            })      
        })
        .then(response => response.json()) 
}

const mostrarEstado = (id) => {
    return fetch(`${process.env.STATS_SERVER_URL}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body :
            JSON.stringify({
              query: `query { partida (id: "${id}"){estado}}`
            })
        })
        .then(response => response.json())
}

const mandarEstado = (url, estado) => {
  return fetch(url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body :
            JSON.stringify(estado)
      })
      .then(response => response.json())
}


const guardarEstado = (estado) => {
  return fetch(`${process.env.STATS_SERVER_URL}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body :
          JSON.stringify({
           query: `mutation {cambiarEstado(id: "${id}", key: "${key}", estado:"${estado}")}`
          })        
      })
      .then(response => response.json())
}


module.exports={crearPartida, mostrarEstado, mandarEstado, guardarEstado}