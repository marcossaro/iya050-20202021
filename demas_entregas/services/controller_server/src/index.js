const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");

const app = new Koa();
const router = new Router();

const fetch = require('node-fetch');
const fetchs = require('./fetchs.js');

let estadoBD = " "; 

router.post("/game", (ctx) => {
  // 1. post mutation to Stats server to store new game
  // 2. return state

  const datos = ctx.request.body;
  estadoBD = datos.estado;
  console.log(estadoBD)

  return fetchs.crearPartida(datos.id, datos.nombre, datos.key, datos.fecha, datos.estado)
  .then((response) => {
    ctx.response.set("Content-Type", "application/json");
    ctx.body = response;
  })

});

router.get("/game/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. return state

  const id = ctx.params.id;

  //console.log("He entrado en game/id")

  ctx.body = estadoBD; 
  //console.log(ctx.body)

  /*
  return fetchs.mostrarEstado(id)
  .then((response) => {
    
    const datos = response.partida.estado;
    console.log(datos)  

    //estadoBD = JSON.parse(datos);
    
    ctx.body = estadoBD; 
    console.log(ctx.body)

  })
  */
});

router.post("/game/:id/:event", (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state

  estadoBD = ctx.request.body;

  const id = ctx.params.id;

  const urlNormas = `${process.env.STATS_SERVER_URL}/norma/${ctx.params.event}`;

  //console.log("He entregado en /game/" + ctx.params.id + "/" + ctx.params.event)
  //console.log(estadoBD)

  //return fetchs.mostrarEstado(id)
  //.then((response) => {
    //ctx.response.set("Content-Type", "application/json"); //esto igual va abajo
    //ctx.body = response.data;
    //estadoBD = ctx.body;

    return fetchs.mandarEstado(urlNormas, estadoBD)
    .then((res) => {
      ctx.response.set("Content-Type", "application/json");   
      ctx.body = res;

      console.log(ctx.body);

      estadoBD = ctx.body;

      /*
      return fetchs.guardarEstado(estadoBD)
      .then((response) => {
      ctx.response.set("Content-Type", "application/json");
      ctx.body = response.data;

      console.log("He entregado en guardar estado")
      })
      */
    });
  //})
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT);
console.log("Servidor controlador");
