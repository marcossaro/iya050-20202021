import React, { useContext, useRef  } from "react";
import AuthContext from "../AuthContext.js";
import { v4 as uuidv4 } from 'uuid';
import { TaskTimer } from 'tasktimer';
import Styles from "../forms.css";
const fetchs = require('../fetchs.js');

const Play = () => {
  const { user } = useContext(AuthContext);

  var estadoBD = " ";
  var numAleatorio = '200';

  const timer = new TaskTimer(1000);

  const crearPartida = () => {
    const estado = {
      "x": "0",
      "y": "0",
      "puntos": "0",
      "gameOver": false,
      "defensores": [],
      "balas": [],
      "enemigos": []
    }

    const fecha = new Date().toLocaleString();

    const datos = {
      id : uuidv4(),
      nombre : user.name,
      key : user.key,
      fecha : fecha,
      estado : estado
    }
    
    return fetchs.enviarDatos(datos)
    .then((response) => console.log(response))
  }

  const guardarPartida = () => {
    const estado = {
      "x": "0",
      "y": "0",
      "puntos": estadoBD.puntos,
      "gameOver": estadoBD.gameOver,
      "defensores": estadoBD.defensores,
      "balas": estadoBD.balas,
      "enemigos": estadoBD.enemigos
    }

    const fecha = new Date().toLocaleString();

    const datos = {
      id : uuidv4(),
      nombre : user.name,
      key : user.key,
      fecha : fecha,
      estado : estado
    }
    
    return fetchs.enviarDatos(datos)
    .then((response) => console.log(response))
  }

  let defensor = (evento) => {
    
    return fetchs.obtenerEstado(user.key)
    .then((response) => {
      estadoBD = response

      estadoBD.x = X.current.value;
      estadoBD.y = Y.current.value;

      console.log(estadoBD)
      console.log(evento)

      return fetchs.normas(user.key, evento, estadoBD)
      .then((response) => {
        console.log(response)
        console.log(response.defensores)
        estadoBD=response;
      })
    })
  }

  let bala = (evento) => {

    return fetchs.obtenerEstado(user.key)
    .then((response) => {
      estadoBD = response

    return fetchs.normas(user.key, evento, estadoBD)
    .then((response) => {
      console.log(response)
      estadoBD=response;
    })
  })
  }

  let enemigo = (evento, x, y) => {

    return fetchs.obtenerEstado(user.key)
    .then((response) => {
      estadoBD = response

    estadoBD.x = x;
    estadoBD.y = y;

    return fetchs.normas(user.key, evento, estadoBD)
    .then((response) => {
      console.log(response)
      estadoBD=response;
    })
  })
  }

  let reloj = (evento) => {

    return fetchs.obtenerEstado(user.key)
    .then((response) => {
      estadoBD = response
      if (estadoBD.gameOver === true){
        timer.stop();
        guardarPartida();
        
      }

      if (timer.tickCount % 11 === 0) {

        var num = Math.random() * (4 - 1) + 1;
        console.log(num)
          if (num <= 2){
            numAleatorio = '100';
          }
          else if (num >= 3){
            numAleatorio = '300';
          } 
          else {
            numAleatorio = '200';
          }

        enemigo('enemigo', '700', numAleatorio);
      }
      if (timer.tickCount % 4 === 0) {
        bala('bala');
      }

    return fetchs.normas(user.key, evento, estadoBD)
    .then((response) => {
      console.log(response)
      estadoBD=response;
    })
  })
  }

  const estado = () => {
    
    return fetchs.obtenerEstado(user.key)
    .then((response) => estadoBD = response)
  }

    const X = useRef();
    const Y = useRef();

    timer.add([
      {
        id: 'task-1', 
        tickInterval: 0.1,  
        callback(task) {        
            reloj('reloj');
            console.log(`${task.id} task has run ${task.currentRuns} times.`);
            const estado = document.getElementById("estado");
            estado.innerHTML = 
            `
            <p>Puntos: ${estadoBD.puntos}</p>
            <p>GameOver: ${estadoBD.gameOver}</p>
            <p>Defensores: ${JSON.stringify(estadoBD.defensores)}</p>
            <p>Balas: ${JSON.stringify(estadoBD.balas)}</p>
            <p>Enemigos: ${JSON.stringify(estadoBD.enemigos)}</p>
            `;
        }
    }
  ]);
   
  timer.on('tick', () => {
      console.log('tick count: ' + timer.tickCount);
      console.log('elapsed time: ' + timer.time.elapsed + ' ms.');
      if (timer.tickCount >= 1000000) timer.stop();
  });

  let startTimer = () => {
    timer.start();
  }

  let stopTimer = () => {
    timer.stop();
  }


  return (
    <div>
      <p>- JUEGO - </p>

      <p id="estado">Estado</p>
      <div className={Styles.cuadrado}></div>

      <form>
            <label>
                X:
                <input type="text" ref={X} />
            </label>
            <label>
                Y:
                <input type="text" ref={Y} />
            </label>
        </form>

      <button onClick={crearPartida}> Crear Partida </button>
      <button onClick={estado}> Cargar Partida </button>
      <button onClick={() => defensor('defensor')}> Defensor </button>
      <button onClick={startTimer}> Iniciar Juego </button>
      <button onClick={stopTimer}> Parar Juego </button>
    </div>
      
    );
};

export default Play;
