import React, { useContext } from "react";
import AuthContext from "../AuthContext.js";
import {mostrarPartida} from '../fetchs.js'
import styles from "../forms.css";

const History = () => {
  const { user } = useContext(AuthContext);

  const historial = () => {
    mostrarPartida(user.key)
    .then(response => mostrar(response))
    
  }

  const mostrar = (response) => {
    const nombre = document.getElementById("nombre");
    nombre.innerHTML = `<p>NOMBRE: ${response.data.partida.nombre}</p>`;

    const idPartida = document.getElementById("id");
    idPartida.innerHTML = `<p>ID: ${response.data.partida.id}</p>`;

    const fechaHora = document.getElementById("fechaHora");
    fechaHora.innerHTML = `<p>FECHA Y HORA: ${response.data.partida.fechaHora}</p>`;

    const puntos = document.getElementById("puntos");
    puntos.innerHTML = `<p>PUNTOS: ${response.data.partida.puntos}</p>`;

    const muertes = document.getElementById("muertes");
    muertes.innerHTML = `<p>MUERTES: ${response.data.partida.muertes}</p>`;

    const tiempo = document.getElementById("tiempo");
    tiempo.innerHTML = `<p>TIEMPO: ${response.data.partida.tiempo}</p>`;

  }

  return (
    <div>
      <meta charSet="utf-8" />    
      
      <div className={styles.cuadrado}>
        <div className={styles.center}>
        <p>HISTORIAL</p>
        </div>
        <ul>
          <li>
            <p id="nombre"></p>
          </li>
          <li>
            <p id="id"></p>
          </li>
          <li>
            <p id="fechaHora"></p>
          </li>
          <li>
            <p id="muertes"></p>
          </li>
          <li>
            <p id="puntos"></p>
          </li>
          <li>
            <p id="tiempo"></p>
          </li>
        </ul>
      </div>
      <button onClick={historial}>Historial</button>
    </div>
  );
};

export default History;
