import React, { useContext } from "react";
import AuthContext from "../AuthContext.js";
import {mostrarPartida} from '../fetchs.js'
import styles from "../forms.css";

const Stats = () => {
  const { user } = useContext(AuthContext);

  const record = () => {
    mostrarPartida(user.key)
    .then(response => mostrar(response))
  }

  const mostrar = (response) => {
    const record = document.getElementById("record");
    record.innerHTML = `<p>${response.data.partida.nombre} tiene el record</p>`;
  }

  return (
    <div>
      <meta charSet="utf-8" />         
      <div className={styles.cuadrado}>
        <div className={styles.center}>
        <p id="record">RECORD</p>
        <div className={styles.podium}></div>
        </div>
      </div>
      <button onClick={record}>Record</button>
    </div>
  );
};



export default Stats;
