const fetch = require('node-fetch');

const enviarDatos = (datos) => {
    return fetch(`${process.env.CONTROLLER_SERVER_URL}/game`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body :
            JSON.stringify(datos)      
        })
        .then(response => response.json()) 
        .then(response => response) 
}

const obtenerEstado = (id) => {
  return fetch(`${process.env.CONTROLLER_SERVER_URL}/game/${id}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        }     
      })
      .then(response => response.json())
      .then(response => response) 
}

const mostrarPartida = (id) => {
  return fetch(`${process.env.STATS_SERVER_URL}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body :
          JSON.stringify({
            query: `query { partida (id: "${id}"){id, nombre, fechaHora, puntos, muertes, tiempo, record}}`
        })     
      })
      .then(response => response.json()) 
      .then(response => response) 
}

const normas = (id, evento, datos) => {
  return fetch(`${process.env.CONTROLLER_SERVER_URL}/game/${id}/${evento}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body :
          JSON.stringify(datos)   
      })
      .then(response => response.json()) 
      .then(response => response) 
}

module.exports={enviarDatos, obtenerEstado, mostrarPartida, normas}