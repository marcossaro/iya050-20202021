const { expect } = require("chai");
const fetch = require('node-fetch');
const { defensorT, balaT, enemigoT, relojT } = require("../normas.js");

const url = "http://localhost:3000/";

//test defensor
describe("normas", () => {
  describe("defensor", () => {
    describe("dando el estado de la partida", () => {
      it("deberia crear un defensor", () => {
        const expected = 
        {
          "x": "200",
          "y": "700",
          "puntos": "0",
          "gameOver": false,
          "defensores": [
            {
              "x": "200",
              "y": "300",
              "disparando": true,
              "vida": "100"
            },
            {
              "x": "200",
              "y": "500",
              "disparando": false,
              "vida": "100"
            },
            {
              "x": "200",
              "y": "700",
              "disparando": false,
              "vida": 100
            }
          ],
          "balas": [],
          "enemigos": [
            {
              "x": "700",
              "y": "300",
              "vida": "100"
            }
          ]
        };

        const actual = defensorT (
          {
            "x": "200",
            "y": "700",
            "puntos": "0",
            "gameOver": false,
            "defensores": [
              {
                "x": "200",
                "y": "300",
                "disparando": true,
                "vida": "100"
              },
              {
                "x": "200",
                "y": "500",
                "disparando": false,
                "vida": "100"
              }
            ],
            "balas": [],
            "enemigos": [
              {
                "x": "700",
                "y": "300",
                "vida": "100"
              }
            ]
          }
        );
        expect(actual).to.deep.equal(expected);
      });
    });
  });
});

//test bala
describe("normas", () => {
  describe("bala", () => {
    describe("dando el estado de la partida", () => {
      it("deberia crear una bala si un defensor esta disparando", () => {
        const expected = 
          {
            "x": "200",
            "y": "700",
            "puntos": "0",
            "gameOver": false,
            "defensores": [
              {
                "x": "200",
                "y": "300",
                "disparando": true,
                "vida": "100"
              },
              {
                "x": "200",
                "y": "500",
                "disparando": false,
                "vida": "100"
              }
            ],
            "balas": [
              {
                "x": "200",
                "y": "300",
                "daño": 20
              }
            ],
            "enemigos": [
              {
                "x": "700",
                "y": "300",
                "vida": "100"
              }
            ]
        };

        const actual = balaT (
            {
              "x": "200",
              "y": "700",
              "puntos": "0",
              "gameOver": false,
              "defensores": [
                {
                  "x": "200",
                  "y": "300",
                  "disparando": true,
                  "vida": "100"
                },
                {
                  "x": "200",
                  "y": "500",
                  "disparando": false,
                  "vida": "100"
                }
              ],
              "balas": [],
              "enemigos": [
                {
                  "x": "700",
                  "y": "300",
                  "vida": "100"
                }
              ]
            }
        );
        expect(actual).to.deep.equal(expected);
      });
    });
  });
});

//test enemigo
describe("normas", () => {
  describe("enemigo", () => {
    describe("dando el estado de la partida", () => {
      it("deberia crear un enemigo", () => {
        const expected = 
          {
            "x": "200",
            "y": "700",
            "puntos": "0",
            "gameOver": false,
            "defensores": [
              {
                "x": "200",
                "y": "300",
                "disparando": true,
                "vida": "100"
              },
              {
                "x": "200",
                "y": "500",
                "disparando": false,
                "vida": "100"
              }
            ],
            "balas": [],
            "enemigos": [
              {
                "x": "700",
                "y": "300",
                "vida": "100"
              },
              {
                "x": "200",
                "y": "700",
                "vida": 100
              }
            ]
          };

        const actual = enemigoT (
            {
              "x": "200",
              "y": "700",
              "puntos": "0",
              "gameOver": false,
              "defensores": [
                {
                  "x": "200",
                  "y": "300",
                  "disparando": true,
                  "vida": "100"
                },
                {
                  "x": "200",
                  "y": "500",
                  "disparando": false,
                  "vida": "100"
                }
              ],
              "balas": [],
              "enemigos": [
                {
                  "x": "700",
                  "y": "300",
                  "vida": "100"
                }
              ]
            }
        );
        expect(actual).to.deep.equal(expected);
      });
    });
  });
});

//test reloj
describe("normas", () => {
  describe("reloj", () => {
    describe("dando el estado de la partida", () => {
      it("deberia mover al enemigo, la bala y quitarle vida al defensor", () => {
        const expected = 
        {
          "x": "200",
          "y": "700",
          "puntos": "0",
          "gameOver": false,
          "defensores": [
            {
              "x": "200",
              "y": "300",
              "disparando": true,
              "vida": 99
            },
            {
              "x": "200",
              "y": "500",
              "disparando": false,
              "vida": "100"
            }
          ],
          "balas": [
            {
              "x": 201,
              "y": "300",
              "daño": 20
            }
          ],
          "enemigos": [
            {
              "x": 701,
              "y": "300",
              "vida": "100"
            }
          ]
        };

        const actual = relojT (
          {
            "x": "200",
            "y": "700",
            "puntos": "0",
            "gameOver": false,
            "defensores": [
              {
                "x": "200",
                "y": "300",
                "disparando": true,
                "vida": "100"
              },
              {
                "x": "200",
                "y": "500",
                "disparando": false,
                "vida": "100"
              }
            ],
            "balas": [
              {
                "x": "200",
                "y": "300",
                "daño": 20
              }
            ],
            "enemigos": [
              {
                "x": "700",
                "y": "300",
                "vida": "100"
              }
            ]
        }
        );
        expect(actual).to.deep.equal(expected);
      });
    });
  });
});


const crearDefensorTest = (body) => {
  //console.log("entre en crearDefensorTest");
  fetch(`${url}/norma/defensor`,
      {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: JSON.stringify(body)
      })
      .then(response => response.json(), console.log("Defensor OK"))
      .catch( error => "createAPair : error");
}

const crearBalaTest = (body) => {
  //console.log("entre en crearBalaTest");
  fetch(`${url}/norma/bala`,
      {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: JSON.stringify(body)
      })
      .then(response => response.json(), console.log("Bala OK"))
      .catch( error => "createAPair : error");
}

const crearEnemigoTest = (body) => {
  //console.log("entre en crearEnemigoTest");
  fetch(`${url}/norma/enemigo`,
      {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: JSON.stringify(body)
      })
      .then(response => response.json(), console.log("Enemigo OK"))
      .catch( error => "createAPair : error");
}

const crearRelojTest = (body) => {
  //console.log("entre en crearRelojTest");
  fetch(`${url}/norma/reloj`,
      {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: JSON.stringify(body)
      })
      .then(response => response.json(), console.log("Reloj OK"))
      .catch( error => "createAPair : error");
}



const body = {
  "x": "200",
  "y": "700",
  "puntos": "0",
  "gameOver": false,
  "defensores": [
    {
      "x": "200",
      "y": "300",
      "disparando": true,
      "vida": "100"
    },
    {
      "x": "200",
      "y": "500",
      "disparando": false,
      "vida": 100
    }
  ],
  "balas": [],
  "enemigos": [
    {
      "x": "700",
      "y": "300",
      "vida": "100"
    }
  ]
}

crearDefensorTest(body);
crearBalaTest(body);
crearEnemigoTest(body);
crearRelojTest(body);