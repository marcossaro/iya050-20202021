const express = require('express')
//const cors = require('cors')
//const bodyParser  = require ('body-parser') 
const norma = require('./normas.js')
 
const app = express()
 
//En este version de express ya no hace falta body parse https://stackoverflow.com/questions/24330014/bodyparser-is-deprecated-express-4
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
 
app.listen(process.env.PORT, () => {
    console.log("Servidor de juego");
  });

//Crear defensor
app.post('/norma/defensor', (req, res) => {

    //console.log(`Se ha recibido una respuesta con path /norma/defensor`);
    res.json(norma.defensor(req))
    //console.log(`Fin de /norma/defensor`);
})
 
//Crear bala (Cada 10 timer creo una bala si el defensor[i].disparando = true)
app.post('/norma/bala', (req, res) => {
    //console.log(`Se ha recibido una respuesta con path /norma/bala`);
    res.json(norma.bala(req, res))
    //console.log(`Fin de /norma/bala`);
})
 
//Crear enemigo  (Cada x timer (voy reduciendolo) añado un enemigo)
app.post('/norma/enemigo', (req, res) => {
    //console.log(`Se ha recibido una respuesta con path /norma/enemigo`);
    res.json(norma.enemigo(req, res))
    //console.log(`Fin de /norma/enemigo`);
})
 
//Reloj (mueve las balas x++, mueve los enemigos x--, comprueba las colisiones entre enemigos-defensores  balas-enemigos   enemigos[i].y = defensores[i].y => defensores[i].disparando = true, se controlan los puntos, game over cuando enemigos-x=0 o bajas=20)
app.post('/norma/reloj', (req, res) => {
    //console.log(`Se ha recibido una respuesta con path /norma/reloj`);
    res.json(norma.reloj(req, res))
    //console.log(`Fin de /norma/reloj`);
})