// Defensor
class Defensor {
    constructor(x, y){
        this.x = x;
        this.y = y;
        this.disparando = false;
        this.vida = '100';
    }
}
 
const defensor = (req) => {
    //console.log("entre en defensor");
 
    const estadoPartida = req.body;
    estadoPartida.defensores.push(new Defensor(estadoPartida.x, estadoPartida.y));

    return estadoPartida;
}
 
 
// Balas
class Bala {
    constructor(x, y){
        this.x = x;
        this.y = y;
        this.daño = '20';
    }
}
 
const bala = (req) => {
    //console.log("entre en bala");
 
    const estadoPartida = req.body;
    
    for (let i = 0; i < estadoPartida.defensores.length; i++){
        if (estadoPartida.defensores[i].disparando === true){
            let x = estadoPartida.defensores[i].x;
            let y = estadoPartida.defensores[i].y;
            estadoPartida.balas.push(new Bala(x, y));
        }         
    }
    return estadoPartida;
}
  
  
// Enemigo
class Enemigo {
    constructor(x, y){
        this.x = x;
        this.y = y;
        this.vida = '100';
    }
}
 
const enemigo = (req) => {
    //console.log("entre en enemigo");
 
    const estadoPartida = req.body;
    estadoPartida.enemigos.push(new Enemigo(estadoPartida.x, estadoPartida.y));

    return estadoPartida;
}
 
const reloj = (req) => {
    //console.log("entre en reloj");
 
    const estadoPartida = req.body;
 
    for (let i = 0; i < estadoPartida.defensores.length; i++){
 
        for (let l = 0; l < estadoPartida.enemigos.length; l++){
            if (estadoPartida.defensores[i].y === estadoPartida.enemigos[l].y){
                estadoPartida.defensores[i].disparando = true;
            }
        }
    }
    
    for (let i = 0; i < estadoPartida.balas.length; i++){
        estadoPartida.balas[i].x = estadoPartida.balas[i].x +++ 10;
        for (let y = 0; y < estadoPartida.enemigos.length; y++){
            if (estadoPartida.enemigos[y] && estadoPartida.balas[i] && colision(estadoPartida.balas[i], estadoPartida.enemigos[y])){
                estadoPartida.enemigos[y].vida = estadoPartida.enemigos[y].vida - estadoPartida.balas[i].daño;
                estadoPartida.balas.splice(i, 1);
                i--;
            }
        };
        if (estadoPartida.balas[i] && estadoPartida.balas[i].x > '700'){
            estadoPartida.balas.splice(i, 1);
            i--;
        }
    }
 
    for (let i = 0; i < estadoPartida.enemigos.length; i++){
        estadoPartida.enemigos[i].x = estadoPartida.enemigos[i].x - 10;
        for (let y = 0; y < estadoPartida.defensores.length; y++){
            
            if (estadoPartida.enemigos[i] && estadoPartida.defensores[y] && colision(estadoPartida.defensores[y], estadoPartida.enemigos[i])){
                estadoPartida.defensores[y].vida = estadoPartida.defensores[y].vida - 1;
            }
        };
        if (estadoPartida.enemigos[i] && estadoPartida.enemigos[i].vida <= '0'){
            estadoPartida.enemigos.splice(i, 1);
            i++
            estadoPartida.puntos = estadoPartida.puntos +++ 100;
        }
        if (estadoPartida.enemigos[i] && estadoPartida.enemigos[i].x < '0'){
            estadoPartida.gameOver = true;
        }   
    }
 
    if (estadoPartida.puntos >= 300){
        estadoPartida.gameOver = true;
    }

    return estadoPartida;
}
 
function colision(primero, segundo){
    if( (primero.x === segundo.x) && (primero.y === segundo.y)){
        return true;
    };
}



































const defensorT = (req) => {
    //console.log("entre en defensorT");
 
    const estadoPartida = req;
    estadoPartida.defensores.push(new Defensor(estadoPartida.x, estadoPartida.y));

    return estadoPartida;
}
 
const balaT = (req) => {
    //console.log("entre en balaT");
 
    const estadoPartida = req;
    
    for (let i = 0; i < estadoPartida.defensores.length; i++){
        if (estadoPartida.defensores[i].disparando === true){
            let x = estadoPartida.defensores[i].x;
            let y = estadoPartida.defensores[i].y;
            estadoPartida.balas.push(new Bala(x, y));
        }         
    }
    return estadoPartida;
}

const enemigoT = (req) => {
    //console.log("entre en enemigoT");
 
    const estadoPartida = req;
    estadoPartida.enemigos.push(new Enemigo(estadoPartida.x, estadoPartida.y));

    return estadoPartida;
}

const relojT = (req) => {
    //console.log("entre en reloj");
 
    const estadoPartida = req;
 
    for (let i = 0; i < estadoPartida.defensores.length; i++){
 
        for (let l = 0; l < estadoPartida.enemigos.length; l++){
            if (estadoPartida.defensores[i].y === estadoPartida.enemigos[l].y){
                estadoPartida.defensores[i].disparando = true;
                break;
            } else {
                estadoPartida.defensores[i].disparando = false;
            }
        }
    }
    
    for (let i = 0; i < estadoPartida.balas.length; i++){
        estadoPartida.balas[i].x++;
        for (let y = 0; y < estadoPartida.enemigos.length; y++){
            if (estadoPartida.enemigos[y] && estadoPartida.balas[i] && colision(estadoPartida.balas[i], estadoPartida.enemigos[y])){
                estadoPartida.enemigos[y].vida -= estadoPartida.balas[i].daño;
                estadoPartida.balas.splice(i, 1);
                i--;
            }
        };
        if (estadoPartida.balas[i] && estadoPartida.balas[i].x > 900){
            estadoPartida.balas.splice(i, 1);
            i--;
        }
    }
 
    for (let i = 0; i < estadoPartida.enemigos.length; i++){
        estadoPartida.enemigos[i].x++;
        if (estadoPartida.enemigos[i] && estadoPartida.enemigos[i].vida <= 0){
            estadoPartida.enemigos.splice(i, 1);
            i++
            estadoPartida.puntos = estadoPartida.puntos + 100;
        }
        if (estadoPartida.enemigos[i] && estadoPartida.enemigos[i].x < 0){
            estadoPartida.gameOver = true;
        }
        for (let y = 0; y < estadoPartida.defensores.length; y++){
            if (estadoPartida.enemigos[i] && estadoPartida.defensores[y] && colision(estadoPartida.defensores[y], estadoPartida.enemigos[i])){
                estadoPartida.defensores[y].vida--;
            }
        };
    }
 
    if (estadoPartida.puntos >= 20){
        estadoPartida.gameOver = true;
    }

    return estadoPartida;
}


module.exports={defensor, bala, enemigo, reloj, defensorT, balaT, enemigoT, relojT}
