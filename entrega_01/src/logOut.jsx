import React from "react";

function logOut() {
 
    return (
      <div>
            <button onClick={logOutF}>Log Out</button>
      </div>
          
    )
  }


const logOutF = () => {

  let token = localStorage.getItem('token')
  console.log(token)

  const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/" + token;

  fetch(url,{
    method: 'DELETE',
    headers :{
            Authorization: token
        }
  })
  .then(response => response.json)
  .then(localStorage.clear())
  .catch(error => console.error('Hay un error en el logOut'))
}

export default logOut;



