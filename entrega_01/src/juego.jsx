import React from 'react';
import LogOut from "./logOut.jsx";


function paginamenu () {

        nombre();       
        return (
            
            <div>
              <meta charSet="utf-8" />
              <title>Flappy Bird</title>
              <link rel="stylesheet" href="style.css" />
              <div className="borde-izquierda" />
              <div className="juego">
                <div className="borde-arriba" />
                <div className="cielo">
                  <div className="pajaro" />
                </div>
              </div>
              <div className="suelo-container">
                <div className="suelo" />
                <div id="numero" />
              </div>
              <div className="borde-derecha">
                <div className="cuadrado">
                  <p id="nombre"> - NOMBRE - </p>
                  <p> - SPACE - Saltar</p>
                  <p> - ENTER - Iniciar juego</p>
                  <LogOut />
                </div>
              </div>
              <div id="root" />
            </div>
        );
}


const nombre = () => {

  let token = localStorage.getItem('token')
  console.log(token)

  const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user";

  fetch(url,{
    method: 'GET',
    headers : {
          Authorization: token
        }
  })
  .then(res => {return res.text()})
  .then(response => {
    let nombre = JSON.parse(response).firstName; 
    //console.log(nombre); 
    const mostrarNombre = document.getElementById("nombre");
    mostrarNombre.innerHTML = `<p>- NOMBRE - ${nombre}</p>`;
  })
  .catch(error => console.error('Hay un error en el nombre'))
}

export default paginamenu;
