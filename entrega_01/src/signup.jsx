import React from "react";
import {BrowserRouter as Router} from "react-router-dom";

function metodoSignUp () {

    return (
      <Router>
        <div>
            <h2>Registro</h2>

            <span>Nombre: <input type="text" id="nombre"/> </span> 
            <p/>
            <span>Nombre de usuario : <input type="text" id="nombreUsuario" /> </span>       
            <p/>
            <span>Contraseña: <input type="password" id="contraseña" /> </span>
            <p/>

            <button onClick={registrarse}>Registrarse</button>

        </div>      
      </Router>
    )
}

const registrarse = () => {

  const contraseñatxt = document.getElementById('contraseña');
  let contraseña = contraseñatxt.value;
  console.log(contraseña)

  const nombreUsuariotxt = document.getElementById('nombreUsuario');
  let nombreUsuario = nombreUsuariotxt.value;
  console.log(nombreUsuario)

  const nombretxt = document.getElementById('nombre');
  let nombre = nombretxt.value;
  console.log(nombre)

  const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + nombreUsuario;

  fetch(url,{
      method: 'POST',
      body :
          JSON.stringify({
              firstName : nombre,
              password : contraseña
          })
  })
  .then(response => response.json)
  .then(data => console.log(data))
  .catch(error => console.error('Hay un error al registrarse'))
}

export default metodoSignUp;