import React from "react";

function metodoSignIn () {
 
    return (
      <div>
            <h2>Iniciar Sesión</h2>

            <span>Nombre de usuario : <input type="text" id="nombreUsuario" /> </span>       
            <p/>
            <span>Contraseña: <input type="password" id="contraseña" /> </span>
            <p/>
            <button onClick={loguearse}>Iniciar Sesión</button>
      </div>
          
    )
}




const loguearse = () => {

  const contraseñatxt = document.getElementById('contraseña');
  let contraseña = contraseñatxt.value;
  console.log(contraseña)

  const nombreUsuariotxt = document.getElementById('nombreUsuario');
  let nombreUsuario = nombreUsuariotxt.value;
  console.log(nombreUsuario)

  const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + nombreUsuario + "/sessions";

  fetch(url,{
    method: 'POST',
    body :
        JSON.stringify({
            password : contraseña
        })
  })
  .then(res => {return res.text()})
  .then(response => {
    let token = JSON.parse(response).sessionToken; 
    console.log(token);
    localStorage.setItem('token', token)
  })
  .catch(error => console.error('Hay un error en el login'))

}

export default metodoSignIn;



