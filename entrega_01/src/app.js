document.addEventListener('DOMContentLoaded' , () => {
    const pajaro = document.querySelector('.pajaro')
    const gameDisplay = document.querySelector('.juego')
    const suelo = document.querySelector('.suelo')

    let pajaroIzquierda = 220
    let pajaroAbajo = 250
    let g = 3
    let isGameOver = false
    let gap = 430
    var n = 0
    var l = document.getElementById("numero")

    function startGame() {
        pajaroAbajo -= g
        pajaro.style.bottom = pajaroAbajo + 'px'
        pajaro.style.left = pajaroIzquierda + 'px'
    }
    let gameTimerId = setInterval(startGame, 20)

    function control(e) {
        if (e.keyCode === 32 && !isGameOver) {
            saltar()
        }
        if (e.keyCode === 13) {
            location.reload()
        }
    }

    function saltar() {
        if (pajaroAbajo < 500) pajaroAbajo += 50
        pajaro.style.bottom = pajaroAbajo + 'px'
    }
    document.addEventListener('keyup', control)


    function generarTubos() {
        let obstaculoIzquierda = 500
        let alturaAleatoria = Math.random() * 100
        let tuboAbajo = alturaAleatoria
        const tubo = document.createElement('div')
        const tuboArriba = document.createElement('div')
        if (!isGameOver) {
            tubo.classList.add('obstaculo')
            tuboArriba.classList.add('obstaculo-arriba')
            l.innerHTML = n
            n++
        }
        gameDisplay.appendChild(tubo)
        gameDisplay.appendChild(tuboArriba)
        tubo.style.left = obstaculoIzquierda + 'px'
        tuboArriba.style.left = obstaculoIzquierda + 'px'
        tubo.style.bottom = tuboAbajo + 'px'
        tuboArriba.style.bottom = tuboAbajo + gap + 'px'

        function moverTubos() {
            obstaculoIzquierda -=2
            tubo.style.left = obstaculoIzquierda + 'px'
            tuboArriba.style.left = obstaculoIzquierda + 'px'

            if (obstaculoIzquierda === -60) {
                clearInterval(timerId)
                gameDisplay.removeChild(tubo)
                gameDisplay.removeChild(tuboArriba)
            }
            if (obstaculoIzquierda > 200 && obstaculoIzquierda < 280 && (pajaroAbajo < tuboAbajo + 153 || pajaroAbajo > tuboAbajo + gap - 200) || pajaroAbajo < 0 ) {
                clearInterval(timerId)
                gameOver()
            }
        }
        let timerId = setInterval(moverTubos, 20)
        if (!isGameOver) setTimeout(generarTubos, 3000)
    }
    generarTubos()



    function gameOver() {
        clearInterval(gameTimerId)
        console.log('game over')
        isGameOver = true
        suelo.classList.add('suelo')
    }
    
})
