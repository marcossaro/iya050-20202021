import React from "react";
import ReactDOM from "react-dom";
import './style.css';
import './app.js';
import MetodoSignUp from "./signup.jsx";
import MetodoSignIn from "./signin.jsx";
import Paginamenu from "./juego.jsx";
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from "react-router-dom";

//Inteté hacer los redirect con useHistory pero no lo conseguí asi que dejé el boton Menu para poder acceder al juego

const App = () => (
    <Router>
        <div>
        <Route exact path="/">
            <Redirect to="/signin" />
        </Route>
          <Switch>
            <Route path="/signin">
              <MetodoSignIn />
              <br/>
              ¿No tienes una cuenta? <Link to="/signup">Registrarse</Link>
            </Route>
            <Route path="/signup">
              <MetodoSignUp />
              <br/>
              ¿Ya tienes una cuenta? <Link to="/signin">Iniciar sesión</Link>
            </Route>
            <Route path="/menu">
              <Paginamenu />
            </Route>
          </Switch>
          
          <br></br>
          <br></br>
          <Link to="/menu">Menu</Link>

        </div>
      </Router>
);

ReactDOM.render(<App />,document.getElementById('root'));
